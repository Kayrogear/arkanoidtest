﻿using Game.Core;
using UnityEngine;

namespace Game
{
    public class InputController : MonoSingleton<InputController>
    {
        public float MoveToX { get; set; }

        public bool Action { get; set; }

        public float MouseSensitivity = 0.3f;

        private bool _isPressed = false;

        private void Start()
        {
            MoveToX = 0.0f;
        }

        private void FixedUpdate()
        {
            Action = false;
            if (Input.GetMouseButton(0) && !_isPressed)
            {
                // mouse or touch down event in fixed frame loop
                _isPressed = true;
            }
            else if (!Input.GetMouseButton(0) && _isPressed)
            {
                // mouse or touch up event in fixed frame loop
                _isPressed = false;
                Action = true;
            }

        }

        private void LateUpdate()
        {
            var playField = WorldSize.Instance.PlayFieldUnits;

#if UNITY_ANDROID && !UNITY_EDITOR

        if (Input.touchCount != 0)
        {
            var touch = Input.touches[0];
            var worldTouchPos = WorldSize.Instance.WorldCamera.ScreenToWorldPoint(touch.position);
            MoveToX = worldTouchPos.x;
        }
                
#else
            var dx = Input.GetAxis("Mouse X") * MouseSensitivity;
            MoveToX += dx;
#endif

            MoveToX = Mathf.Clamp(MoveToX, playField.min.x, playField.max.x);
        }

    }
}
