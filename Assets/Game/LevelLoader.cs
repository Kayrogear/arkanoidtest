﻿using System;
using UnityEngine;
using System.Linq;
using Game.Core;
using Game.Messages;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public int LevelsCount { get { return _levelsNames.Length; } }

    public bool InMenuScene
    {
        get { return SceneManager.GetActiveScene().name == _mainMenuName; }
    }

    public bool InLevelScene
    {
        get
        {
            var currentSceneName =  SceneManager.GetActiveScene().name;
            return _levelsNames.Any(name => name == currentSceneName);
        }
    }

    public bool IsLastLevel
    {
        get { return _levelIndex == _levelsNames.Length - 1; }
    }

    [SerializeField]
    private string _mainMenuName;

    [SerializeField]
    private string[] _levelsNames;

    private int _levelIndex = -1;

    public string GetLevelName(int index)
    {
        return _levelsNames[index];
    }

    public void LoadMainMenu()
    {
        _levelIndex = -1;
        SceneManager.LoadScene(_mainMenuName);
    }

    public void ReloadLevel()
    {
        if (_levelIndex < 0) return;
        LoadLevel(_levelIndex);
    }

    public void LoadLevel(int index)
    {
        _levelIndex = index;
        SceneManager.LoadScene(_levelsNames[index]);
    }

    public bool LoadNextLevel()
    {
        _levelIndex++;
        if (_levelIndex >= _levelsNames.Length)
            return false;
        LoadLevel(_levelIndex);
        return true;
    }

    private void Awake()
    {
        _levelIndex = Array.IndexOf(_levelsNames, SceneManager.GetActiveScene().name);
        SceneManager.sceneLoaded += SceneManager_SceneLoaded;
    }
    
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= SceneManager_SceneLoaded;
    }

    private void SceneManager_SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        var index = Array.IndexOf(_levelsNames, scene.name);
        MessagesSystem.Send(this, new SceneLoadedMsg(InMenuScene, index));
    }
}
