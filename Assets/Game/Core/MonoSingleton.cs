﻿using UnityEngine;

namespace Game.Core
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                    if (_instance == null)
                    {
                        var name = typeof (T).Name;
                        var go = new GameObject("_" + name, typeof (T));
                        _instance = go.GetComponent<T>();
                    }
                }
                return _instance;
            }
        }

        protected virtual void Awake()
        {
            if (_instance == null) _instance = this as T;
            if (_instance != null && !ReferenceEquals(_instance, this))
            {
                DestroyImmediate(gameObject);
                return;
            }

            Debug.Log("Create Singleton " + this);
            OnInit();
        }

        protected virtual void OnInit()
        {
        }
    }
}