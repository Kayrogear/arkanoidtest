﻿namespace Game.Core
{
    public abstract class MessageBase
    {
        public object Sender { get; set; }
    }
}
