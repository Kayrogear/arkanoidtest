﻿using System;
using System.Collections.Generic;

namespace Game.Core
{
    public static class MessagesSystem
    {
        public delegate void EventDelegate<T>(object sender, T m) where T : MessageBase;
        private delegate void EventDelegate(object sender, MessageBase m);

        private static Dictionary<Delegate, EventDelegate> _lookup = new Dictionary<Delegate, EventDelegate>();
        private static Dictionary<Type, List<EventDelegate>> _delegatesDict = new Dictionary<Type, List<EventDelegate>>(); 

        public static void AddListener<T>(EventDelegate<T> d) where T : MessageBase
        {
            if (_lookup.ContainsKey(d)) return;

            EventDelegate internalDelegate = (sender, message) => d(sender, (T)message);
            _lookup[d] = internalDelegate;

            Type t = typeof(T);
            if (!_delegatesDict.ContainsKey(t))
                _delegatesDict[t] = new List<EventDelegate>();

            var list = _delegatesDict[t];
            list.Add(internalDelegate);
        }

        public static void RemoveListener<T>(EventDelegate<T> d) where T : MessageBase
        {
            EventDelegate internalDelegate;
            if (!_lookup.TryGetValue(d, out internalDelegate)) return;

            _lookup.Remove(d);
            Type t = typeof(T);
            var list = _delegatesDict[t];
            list.Remove(internalDelegate);

            if (list.Count == 0)
                _delegatesDict.Remove(t);
        }

        public static void RemoveAllListeners()
        {
            _lookup.Clear();
            _delegatesDict.Clear();
        }

        public static bool HasListener<T>(EventDelegate<T> d) where T : MessageBase
        {
            return _lookup.ContainsKey(d);
        }

        public static void Send(object sender, MessageBase m)
        {
            if (!_delegatesDict.ContainsKey(m.GetType())) return;
            m.Sender = sender;
            var delegatesArray = _delegatesDict[m.GetType()].ToArray();
            foreach (var d in delegatesArray)
                d.Invoke(sender, m);
        }
    }
}

