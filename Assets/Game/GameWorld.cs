﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Actors;
using Game.Actors.Bricks;
using Game.Actors.Messages;
using Game.Core;
using Game.Messages;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class GameWorld : MonoSingleton<GameWorld>
    {
        public IEnumerable<Ball> Balls { get { return _balls; } }

        public Platform Platform { get { return _platform; } }

        public bool IsPaused
        {
            get { return _isPaused; }
            set
            {
                _isPaused = value;
                Time.timeScale = _isPaused ? 0.0f : 1.0f;
                MessagesSystem.Send(this, new GameWorldPausedMsg(_isPaused));
            }
        }

        public GameWorldState State { get { return _stateMachine.CurrentStateId ?? GameWorldState.Start; } }

        [SerializeField]
        private int _playerLives = 3;

        [SerializeField]
        private GameObject _explosion1Prefab;

        [SerializeField]
        private Ball _ballPrefab;

        [SerializeField]
        private Platform _platformPrefab;
        
        private StateMachine<GameWorldState> _stateMachine = new StateMachine<GameWorldState>();

        private List<Brick> _bricks = new List<Brick>();
        private List<Ball> _balls = new List<Ball>();
        private Platform _platform;
        private bool _isPaused;
        
        protected override void OnInit()
        {
            _stateMachine.StateChangeEnd += (prevState, newState) =>
            {
                MessagesSystem.Send(this, new GameStateChangedMsg(prevState, newState));
                Debug.LogFormat("World State {0}->{1}", prevState, newState);
            };

            _stateMachine.In(GameWorldState.Start)
                .ExecuteEnter(OnStartEnter)
                .Goto(GameWorldState.Play);

            _stateMachine.In(GameWorldState.Play)
                .ExecuteEnter(OnPlayEnter)
                .Goto(GameWorldState.AllBallsLost)
                .Goto(GameWorldState.AllBricksDestroyed);

            _stateMachine.In(GameWorldState.AllBallsLost)
                .ExecuteEnter(OnAllBallsLostEnter)
                .Goto(GameWorldState.Start)
                .Goto(GameWorldState.GameOver);

            _stateMachine.In(GameWorldState.AllBricksDestroyed)
                .ExecuteEnter(OnAllBricksDestroyedEnter)
                .Goto(GameWorldState.LevelComplete);

            _stateMachine.In(GameWorldState.LevelComplete)
                .ExecuteEnter(OnLevelCompleteEnter);

            _stateMachine.In(GameWorldState.GameOver)
                .ExecuteEnter(OnGameOverEnter);

            MessagesSystem.AddListener<GameActorCreatedMsg>(OnGameActorCreated);
            MessagesSystem.AddListener<GameActorDestroyedMsg>(OnGameActorDestroyed);
        }
        
        private void OnDestroy()
        {
            MessagesSystem.RemoveListener<GameActorCreatedMsg>(OnGameActorCreated);
            MessagesSystem.RemoveListener<GameActorDestroyedMsg>(OnGameActorDestroyed);
        }

        private void Start()
        {
            IsPaused = false;

            var player = GameApp.Instance.Player;
            player.Lives = _playerLives;

            MessagesSystem.Send(this, new PlayerDataChangedMsg(GameApp.Instance.Player));
            _stateMachine.Start(GameWorldState.Start);
        }

        private void Update()
        {
            if (_stateMachine.CurrentStateId.HasValue)
            {
                switch (_stateMachine.CurrentStateId.Value)
                {
                    case GameWorldState.Start:
                        OnStartUpdate();
                        break;
                    case GameWorldState.Play:
                        OnPlayUpdate();
                        break;
                }
            }
        }
    
        private Platform FindOrCreatePlatform()
        {
            var platform = FindObjectOfType<Platform>();
            if (platform == null)
                platform = (Platform)Instantiate(_platformPrefab, transform);
            return platform;
        }

        private Ball FindOrCreateBall()
        {
            var ball = FindObjectOfType<Ball>();
            if (ball == null)
                ball = (Ball)Instantiate(_ballPrefab, transform);
            return ball;
        }

        public Ball GetRandomBall()
        {
            if (_balls.Count == 0) return null;
            var randIndex = Random.Range(0, _balls.Count - 1);
            return _balls[randIndex];
        }

        public IEnumerable<Ball> CreateBalls(Vector2 worldPosition, int count, bool launched = true)
        {
            var balls = new List<Ball>();
            for (var i = 0; i < count; i++)
            {
                var ball = (Ball)Instantiate(_ballPrefab, transform, true);
                ball.transform.position = worldPosition;
                ball.Lauched = launched;
                balls.Add(ball);
            }
            return balls;
        } 

        #region States Subroutines

        private void OnStartEnter()
        {
            var ball = FindOrCreateBall();
            _platform = FindOrCreatePlatform();
            _platform.ResetToDefault();
            _platform.DockBall(ball);
        }

        private void OnStartUpdate()
        {
            foreach (var ball in Balls)
            {
                if (!ball.Lauched) continue;
                _stateMachine.SwitchState(GameWorldState.Play);
                break;
            }
        }

        private void OnPlayEnter()
        {
        
        }

        private void OnPlayUpdate()
        {
            if (_balls.Count == 0)
                _stateMachine.SwitchState(GameWorldState.AllBallsLost);

            if (_bricks.Count == 0 || _bricks.All(b=>!b.IsDestroyable))
                _stateMachine.SwitchState(GameWorldState.AllBricksDestroyed);
        }

        private void OnAllBallsLostEnter()
        {
            var player = GameApp.Instance.Player;
            player.Lives--;
            MessagesSystem.Send(this, new PlayerDataChangedMsg(player));
            if (player.Lives > 0)
            {
                // use live
                StartCoroutine(WaitAndExecute(1.0f, () =>
                {
                    _stateMachine.SwitchState(GameWorldState.Start);
                }));

            }
            else
            {
                // game over
                StartCoroutine(WaitAndExecute(1.0f, () =>
                {
                    _stateMachine.SwitchState(GameWorldState.GameOver);
                }));
            }
        }

        private void OnAllBricksDestroyedEnter()
        {
            _balls.ForEach(b=>b.Stop());
            StartCoroutine(WaitAndExecute(1.0f, () =>
            {
                _stateMachine.SwitchState(GameWorldState.LevelComplete);
            }));
        }

        private void OnGameOverEnter()
        {
            MessagesSystem.Send(this, new GameWorldPlayEndMsg(false));
        }

        private void OnLevelCompleteEnter()
        {
            MessagesSystem.Send(this, new GameWorldPlayEndMsg(true));
        }

        #endregion

        IEnumerator WaitAndExecute(float seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);
            action();
        }

        private void OnGameActorCreated(object sender, GameActorCreatedMsg m)
        {
            if (m.Actor is Ball)
            {
                var ball = (Ball) m.Actor;
                if (_balls.Contains(ball)) return;
                _balls.Add(ball);
            }
            else if (m.Actor is Brick)
            {
                var brick = (Brick)m.Actor;
                if (_bricks.Contains(brick)) return;
                _bricks.Add(brick);
            }

            if (!m.Object.transform.IsChildOf(transform))
                m.Object.transform.SetParent(transform, true);
        }

        private void OnGameActorDestroyed(object sender, GameActorDestroyedMsg m)
        {
            if (m.Actor is Ball)
            {
                _balls.Remove((Ball) m.Actor);
            }
            else if (m.Actor is Brick)
            {
                var brick = (Brick) m.Actor;
                _bricks.Remove(brick);
                var explosion = (GameObject)Instantiate(_explosion1Prefab, transform);
                explosion.transform.localPosition = m.Actor.transform.localPosition;
            }

            if (m.Reason == GameActor.DestroyReason.ByPlayer && m.Actor is IHasScoreValue)
                AddPlayerScores(m.Actor, (IHasScoreValue)m.Actor);
        }

        private void AddPlayerScores(GameActor actor, IHasScoreValue value)
        {
            if (value.Scores <= 0) return;

            var player = GameApp.Instance.Player;
            player.Scores += value.Scores;

            MessagesSystem.Send(this, new PlayerDataChangedMsg(GameApp.Instance.Player));
            MessagesSystem.Send(this, new PlayerScoresChangedMsg(player, value.Scores, actor));
        }
    }
}