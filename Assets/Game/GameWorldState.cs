﻿namespace Game
{
    public enum GameWorldState
    {
        Start,
        Play,
        AllBallsLost,
        AllBricksDestroyed,
        GameOver,
        LevelComplete
    }
}