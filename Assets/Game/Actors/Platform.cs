﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Actors
{
    public class Platform : GameActor
    {
        public enum Size
        {
            Small,
            Mid,
            Big    
        }

        public float Angle = 35;
        public float YOffset = 0.1f;
        public float DockedBallOffset = 0.01f;
        public float MouseSensitivity = 1;

        [SerializeField]
        private GameObject _platformSmall;

        [SerializeField]
        private GameObject _platformMid;

        [SerializeField]
        private GameObject _platformBig;

        private Size _size = Size.Mid;
        private GameObject _currentSize;
        private Collider2D _collider;
        private Vector2 _lastNormalPoint;
        private Vector2 _lastNormal;
        private List<Ball> _dockedBalls = new List<Ball>();

        public Vector2 GetNormal(Vector2 worldPoint)
        {
            var x = transform.worldToLocalMatrix.MultiplyPoint(worldPoint).x;
            var k = Mathf.Clamp(x / _collider.bounds.extents.x, -1, 1);
            var angle = Angle * k;
            var n = Quaternion.Euler(0, 0, -angle) * Vector3.up;

            _lastNormal = n;
            _lastNormalPoint = worldPoint;

            return n;
        }

        public void DockBall(Ball ball, bool random = true)
        {
            if (_dockedBalls.Contains(ball)) return;
            ball.Lauched = false;

            var x = random
                ? Random.Range(-0.9f, 0.9f) * _collider.bounds.extents.x
                : 0.0f;

            ball.transform.SetParent(transform);
            ball.transform.localPosition = new Vector3(x, ball.Radius * 2 + DockedBallOffset, 0);

            _dockedBalls.Add(ball);
        }

        public void LaunchBalls()
        {
            foreach (var ball in _dockedBalls)
            {
                if (ball == null) continue;
                ball.transform.SetParent(WorldSize.Instance.transform, true);
                ball.Launch(GetNormal(ball.transform.position));
            }
            _dockedBalls.Clear();
        }

        public void SetSize(Size size)
        {
            _size = size;
            switch (size)
            {
                case Size.Small:
                    _currentSize = _platformSmall;
                    _platformSmall.SetActive(true);
                    _platformMid.SetActive(false);
                    _platformBig.SetActive(false);
                    break;
                case Size.Mid:
                    _currentSize = _platformMid;
                    _platformSmall.SetActive(false);
                    _platformMid.SetActive(true);
                    _platformBig.SetActive(false);
                    break;
                case Size.Big:
                    _currentSize = _platformBig;
                    _platformSmall.SetActive(false);
                    _platformMid.SetActive(false);
                    _platformBig.SetActive(true);
                    break;
            }

            _collider = _currentSize.GetComponent<Collider2D>();
        }

        public void ResetToDefault()
        {
            SetSize(Size.Mid);
        }

        private void Awake()
        {
            ResetToDefault();
        }

        protected override void OnActorDestroy(DestroyReason reason, GameObject destroyer)
        {
            _dockedBalls.Clear();
        }

        private void FixedUpdate()
        {
            var input = InputController.Instance;

            var x = transform.localPosition.x;
            x = Mathf.Lerp(x, InputController.Instance.MoveToX, 0.3f);

            var playField = WorldSize.Instance.PlayFieldUnits;
            
            if (x > playField.max.x - _collider.bounds.extents.x)
                x = playField.max.x - _collider.bounds.extents.x;

            if (x < playField.min.x + _collider.bounds.extents.x)
                x = playField.min.x + _collider.bounds.extents.x;

            var pos = new Vector3(x, playField.min.y + _collider.bounds.extents.y + YOffset, transform.localPosition.z);
            transform.localPosition = pos;

            if (input.Action)
                LaunchBalls();
        }

        private void OnDrawGizmos()
        {
            Debug.DrawLine(_lastNormalPoint, _lastNormalPoint + _lastNormal, Color.red);
        }
    }
}