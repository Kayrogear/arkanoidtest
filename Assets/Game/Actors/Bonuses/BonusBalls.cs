﻿namespace Game.Actors.Bonuses
{
    public class BonusBalls : Bonus
    {
        public override BonusType Type { get { return BonusType.BallClone; } }

        public int NumberOfBalls = 2;

        public override void Apply()
        {
            var world = GameWorld.Instance;
            var ball = world.GetRandomBall();

            if (ball == null) return;
            var clones = ball.MakeClones(NumberOfBalls);
            foreach (var clone in clones)
                clone.Lauched = true;
        }
    }
}
