﻿using Game.Actors.Bonuses;

namespace Game
{
    public class BonusBallsSpeed : Bonus
    {
        public override BonusType Type
        {
            get { return BonusType.BallSpeed; }
        }

        public float SpeedIncrease = 1.0f;
        
        public override void Apply()
        {
            var world = GameWorld.Instance;
            foreach (var ball in world.Balls)
                ball.Speed += SpeedIncrease;
        }
    }
}
