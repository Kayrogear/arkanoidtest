﻿namespace Game.Actors.Bonuses
{
    public enum BonusType
    {
        None,
        PlatformSize,
        BallSpeed,
        BallClone
    }
}
