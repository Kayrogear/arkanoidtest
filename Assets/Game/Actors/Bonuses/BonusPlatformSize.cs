﻿namespace Game.Actors.Bonuses
{
    public class BonusPlatformSize : Bonus
    {
        public override BonusType Type
        {
            get { return BonusType.PlatformSize; }
        }

        public Platform.Size Size;

        public override void Apply()
        {
            var world = GameWorld.Instance;
            world.Platform.SetSize(Size);
        }
    }
}
