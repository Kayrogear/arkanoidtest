﻿using UnityEngine;

namespace Game.Actors.Bonuses
{
    public abstract class Bonus : GameActor
    {
        public abstract BonusType Type { get; }

        public float FallSpeed = 0.4f;

        public abstract void Apply();
        
        private void FixedUpdate()
        {
            var pos = transform.localPosition;
            pos.y -= FallSpeed * Time.fixedDeltaTime;
            transform.localPosition = pos;

            if (pos.y < WorldSize.Instance.LostLevelY)
                ActorDestroy(DestroyReason.ByLostLine);
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject.tag != Tags.Platform) return;
            Apply();
            ActorDestroy(DestroyReason.ByPlayer, collider.gameObject);
        }
    }
}
