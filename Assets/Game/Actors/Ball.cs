﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Actors
{
    [RequireComponent(typeof(CircleCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class Ball : GameActor
    {
        public bool Lauched { get; set; }
        public float Radius { get { return _collider.radius * transform.localScale.x; } }

        public float Speed
        {
            get { return _speed; }
            set { if (!_stopped) _speed = Mathf.Clamp(value, _minSpeed, _maxSpeed); }
        }

        [SerializeField]
        public GameObject _sparclesPrefab;

        [SerializeField]
        private float _speed = 2.0f;

        [SerializeField]
        private float _minSpeed = 3.0f;

        [SerializeField]
        private float _maxSpeed = 6.0f;

        private CircleCollider2D _collider;
        private Rigidbody2D _rigidBody;
        private Vector2 _direction;
        private bool _stopped = false;

        private Vector2 _accumNormal;
        private List<Vector2> _contactPoints = new List<Vector2>();
        private List<GameObject> _inCollision = new List<GameObject>();

        public void Launch(Vector2 direction)
        {
            if (Lauched) return;
            _direction = direction.normalized;
            Lauched = true;
        }

        public IEnumerable<Ball> MakeClones(int count)
        {
            var clones = new List<Ball>();
            for (int i = 0; i < count; i++)
            {
                var parent = GameWorld.Instance.transform;
                var clonedGameObject = (GameObject)Instantiate(gameObject, parent, true);
                var clonedBall = clonedGameObject.GetComponent<Ball>();
                //clonedBall.transform.position = transform.position;
                clones.Add(clonedBall);
            }
            return clones;
        }

        public void Stop()
        {
            _stopped = true;
        }

        private void Awake()
        {
            _collider = GetComponent<CircleCollider2D>();
            _rigidBody = GetComponent<Rigidbody2D>();
            _direction = Random.insideUnitCircle.normalized;
            //Lauched = false;
        }
        
        private void FixedUpdate()
        {
            ApplyCollisions();

            // check world walls collisions
            var playField = WorldSize.Instance.PlayFieldUnits;
            var pos = transform.localPosition;

            if (pos.x < playField.min.x + _collider.radius)
            {
                pos.x = playField.min.x + _collider.radius;
                _direction.x *= -1;
                CreateSparcles(new Vector3(playField.min.x, pos.y, 0), _direction);
            }
            else if (pos.x > playField.max.x - _collider.radius)
            {
                pos.x = playField.max.x - _collider.radius;
                _direction.x *= -1;
                CreateSparcles(new Vector3(playField.max.x, pos.y, 0), _direction);
            }
            

            if (pos.y > playField.max.y - _collider.radius)
            {
                pos.y = playField.max.y - _collider.radius;
                _direction.y *= -1;
                CreateSparcles(new Vector3(pos.x, playField.max.y,0), _direction);
            }

            transform.localPosition = pos;

            if (_stopped) _speed = Mathf.Lerp(_speed, 0, 0.1f);
            _rigidBody.velocity = Lauched ? _direction * _speed : Vector2.zero;

            // check lost
            if (pos.y < WorldSize.Instance.LostLevelY)
                ActorDestroy(DestroyReason.ByLostLine);
        }

        private void ApplyCollisions()
        {
            if (_contactPoints.Count == 0) return;

            _accumNormal.Normalize();
            var d = Vector2.Dot(_accumNormal, _direction);
            _direction = -2 * (_accumNormal * d) + _direction;
            _accumNormal = Vector2.zero;

            foreach (var point in _contactPoints)
                CreateSparcles(point, _direction);

            _contactPoints.Clear();
        }

        private void CreateSparcles(Vector3 pos, Vector2 dir)
        {
            if (_speed <= 0.001f || !Lauched) return;
            var sparcles = (GameObject)Instantiate(_sparclesPrefab, GameWorld.Instance.transform);
            var angle = -Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            sparcles.transform.rotation = Quaternion.AngleAxis(angle + 90, new Vector3(0,0,1));
            sparcles.transform.position = pos;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Collision(collision);
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            Collision(collision);
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            _inCollision.Remove(collision.gameObject);
        }
        
        private void Collision(Collision2D collision)
        {
            if (_inCollision.Contains(collision.gameObject)) return;
            _inCollision.Add(collision.gameObject);
            
            if (collision.gameObject.tag == Tags.Platform)
            {
                var platform = collision.gameObject.GetComponentInParent<Platform>();
                _accumNormal += platform.GetNormal(transform.position);
            }
            else
            {
                foreach (var contact in collision.contacts)
                    _accumNormal += contact.normal;
            }

            _contactPoints.AddRange(collision.contacts.Select(c=>c.point));
        }
    }
}
