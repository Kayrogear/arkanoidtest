﻿using UnityEngine;

namespace Game.Actors.Bricks
{
    public class StatesBrick : Brick
    {
        public override bool IsDestroyable { get { return true; } }

        [SerializeField]
        private GameObject[] _states;
        private int _stateIndex = 0;

        private void SetState(int index)
        {
            if (index < 0 || index >= _states.Length) return;

            foreach (var state in _states)
                state.SetActive(false);
            
            _states[index].SetActive(true);
        }

        private void Awake()
        {
            SetState(_stateIndex);
        }

        protected override void OnCollideWithBall(Ball ball)
        {
            _stateIndex++;
            SetState(_stateIndex);
            if (_states == null || _stateIndex >= _states.Length)
                ActorDestroy(DestroyReason.ByPlayer, ball.gameObject);
        }
    }
}
