﻿using UnityEngine;

namespace Game.Actors.Bricks
{
    [RequireComponent(typeof(Collider2D))]
    public abstract class Brick : GameActor, IHasScoreValue
    {
        public abstract bool IsDestroyable { get; }
        public int Scores { get { return _scoresDestroy; } }
        
        [SerializeField]
        private GameObject _bonusPrefab;

        [SerializeField]
        public int _scoresDestroy = 50;

        protected override void OnActorDestroy(DestroyReason reason, GameObject destroyer)
        {
            if (reason == DestroyReason.ByPlayer && _bonusPrefab != null)
            {
                var bonusGameObject = Instantiate(_bonusPrefab);
                bonusGameObject.transform.position = transform.position;
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == Tags.Ball)
                OnCollideWithBall(collision.gameObject.GetComponent<Ball>());
        }

        protected abstract void OnCollideWithBall(Ball ball);

        
    }
}
