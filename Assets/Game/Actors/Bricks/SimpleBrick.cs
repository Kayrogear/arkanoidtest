﻿namespace Game.Actors.Bricks
{
    public class SimpleBrick : Brick
    {
        public override bool IsDestroyable { get { return true; } }

        protected override void OnCollideWithBall(Ball ball)
        {
            ActorDestroy(DestroyReason.ByPlayer, ball.gameObject);
        }
    }
}
