﻿namespace Game.Actors.Bricks
{
    public class SolidBrick : Brick
    {
        public override bool IsDestroyable { get { return false; } }

        protected override void OnCollideWithBall(Ball ball)
        {
            // nothing...
        }
    }
}
