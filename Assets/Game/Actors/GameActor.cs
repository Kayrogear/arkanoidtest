﻿using Game.Actors.Messages;
using Game.Core;
using UnityEngine;

namespace Game.Actors
{
    public abstract class GameActor : MonoBehaviour
    {
        public enum DestroyReason
        {
            None,
            ByPlayer,
            ByLostLine
        }

        public bool IsDestroyed { get; private set; }

        private void Start()
        {
            OnActorCreate();
            MessagesSystem.Send(this, new GameActorCreatedMsg(this));
        }

        private void OnDestroy()
        {
            InternalDestroy(DestroyReason.None, null, false);
        }

        public void ActorDestroy(DestroyReason reason = DestroyReason.None, GameObject destroyer = null)
        {
            InternalDestroy(reason, destroyer, true);
        }

        private void InternalDestroy(DestroyReason reason, GameObject destroyer, bool destroyGameObject)
        {
            if (IsDestroyed) return;
            IsDestroyed = true;
            MessagesSystem.Send(this, new GameActorDestroyedMsg(this, reason, destroyer));
            OnActorDestroy(reason, destroyer);
            if (destroyGameObject) 
                Destroy(gameObject);
        }

        protected virtual void OnActorCreate() { }
        protected virtual void OnActorDestroy(DestroyReason reason, GameObject destroyer) { }
    }
}
