﻿using Game.Core;
using UnityEngine;

namespace Game.Actors.Messages
{
    public class GameActorCreatedMsg : MessageBase
    {
        public GameActor Actor { get; private set; }
        public GameObject Object { get; private set; }

        public GameActorCreatedMsg(GameActor actor)
        {
            Actor = actor;
            Object = actor.gameObject;
        }
    }
}
