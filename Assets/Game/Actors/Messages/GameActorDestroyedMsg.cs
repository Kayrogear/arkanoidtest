﻿using Game.Core;
using UnityEngine;

namespace Game.Actors.Messages
{
    public class GameActorDestroyedMsg : MessageBase
    {
        public GameActor Actor { get; private set; }
        public GameActor.DestroyReason Reason { get; private set; }
        public GameObject Destroyer { get; private set; }
        
        public GameActorDestroyedMsg(GameActor actor, GameActor.DestroyReason reason, GameObject destroyer)
        {
            Actor = actor;
            Reason = reason;
            Destroyer = destroyer;
        }
    }
}
