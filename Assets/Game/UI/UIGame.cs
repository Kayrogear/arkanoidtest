﻿using Game.Core;
using Game.Messages;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class UIGame : MonoBehaviour
    {
        public UIPopupScores PopupScoresPrefab;

        public GameObject PausePanel;
        public GameObject LevelCompletePanel;
        public GameObject GameOverPanel;
        public GameObject GameCompletePanel;

        public Text ScoresText;
        public Text LivesText;
        private Canvas _canvas;
        private RectTransform _canvasTransform;

        public void PauseGame()
        {
            var gw = GameWorld.Instance;
            gw.IsPaused = !gw.IsPaused;
        }

        public void GotoMainMenu()
        {
            GameApp.Instance.Levels.LoadMainMenu();
        }

        public void RestartLevel()
        {
            GameApp.Instance.Levels.ReloadLevel();
        }

        public void NextLevel()
        {
            GameApp.Instance.Levels.LoadNextLevel();
        }

        public void PopupScores(Vector3 worldPos, int scores)
        {
            var popupScores = (UIPopupScores)Instantiate(PopupScoresPrefab, transform);
            var popupTransform = popupScores.gameObject.GetComponent<RectTransform>();
            popupTransform.anchoredPosition = WorldToCanvasPosition(worldPos);
            popupTransform.localScale = Vector3.one;
            popupScores.Scores = scores;
        }

        private void Awake()
        {
            _canvas = GetComponentInParent<Canvas>();
            _canvasTransform = _canvas.GetComponent<RectTransform>();

            MessagesSystem.AddListener<PlayerDataChangedMsg>(OnPlayerDataChanged);
            MessagesSystem.AddListener<PlayerScoresChangedMsg>(OnPlayerScoresChanged);
            MessagesSystem.AddListener<GameStateChangedMsg>(OnGameStateChanged);
            MessagesSystem.AddListener<GameWorldPausedMsg>(OnGameWorldPaused);
            MessagesSystem.AddListener<GameWorldPlayEndMsg>(OnGameWorldPlayEnd);
        }

        private void OnDestroy()
        {
            MessagesSystem.RemoveListener<PlayerDataChangedMsg>(OnPlayerDataChanged);
            MessagesSystem.RemoveListener<PlayerScoresChangedMsg>(OnPlayerScoresChanged);
            MessagesSystem.RemoveListener<GameStateChangedMsg>(OnGameStateChanged);
            MessagesSystem.RemoveListener<GameWorldPausedMsg>(OnGameWorldPaused);
            MessagesSystem.RemoveListener<GameWorldPlayEndMsg>(OnGameWorldPlayEnd);
        }
        
        private void OnEnable()
        {
            UpdatePanels();
        }
        
        private Vector3 WorldToCanvasPosition(Vector3 worldPos)
        {
            var viewportPos = Camera.main.WorldToViewportPoint(worldPos);
            var pos = new Vector2(
               (viewportPos.x * _canvasTransform.sizeDelta.x) - (_canvasTransform.sizeDelta.x * 0.5f),
               (viewportPos.y * _canvasTransform.sizeDelta.y) - (_canvasTransform.sizeDelta.y * 0.5f));
            return pos;
        }

        private void UpdatePlayerData(PlayerData player)
        {
            ScoresText.text = "SCORES: " + player.Scores.ToString("00000");
            LivesText.text = "x" + player.Lives;
        }

        private void UpdatePanels()
        {
            GameCompletePanel.SetActive(false);
            GameOverPanel.SetActive(false);
            LevelCompletePanel.SetActive(false);
            PausePanel.SetActive(false);

            var gw = GameWorld.Instance;

            if (gw.State == GameWorldState.LevelComplete && GameApp.Instance.Levels.IsLastLevel)
                GameCompletePanel.SetActive(true);
            else if (gw.State == GameWorldState.LevelComplete)
                LevelCompletePanel.SetActive(true);
            else if (gw.State == GameWorldState.GameOver)
                GameOverPanel.SetActive(true);
            else if (gw.IsPaused)
                PausePanel.SetActive(gw.IsPaused);
        }

        private void OnGameWorldPlayEnd(object sender, GameWorldPlayEndMsg m)
        {
            UpdatePanels();
        }

        private void OnPlayerScoresChanged(object sender, PlayerScoresChangedMsg m)
        {
            if (m.ByActor == null) return;
            PopupScores(m.ByActor.gameObject.transform.position, m.ScoresValue);
        }
        
        private void OnGameStateChanged(object sender, GameStateChangedMsg m)
        {
            switch (m.NewState)
            {
                case GameWorldState.Start:
                    break;
                case GameWorldState.Play:
                    break;
                case GameWorldState.AllBallsLost:
                    break;
                case GameWorldState.AllBricksDestroyed:
                    break;
            }
        }

        private void OnGameWorldPaused(object sender, GameWorldPausedMsg m)
        {
            UpdatePanels();
        }

        private void OnPlayerDataChanged(object sender, PlayerDataChangedMsg m)
        {
            UpdatePlayerData(m.Player);
        }
    }
}
