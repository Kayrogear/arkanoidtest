using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    [RequireComponent(typeof(Text))]
    public class UIPopupScores : MonoBehaviour
    {
        public float LifeTimeSec;
        public int Scores;

        private float _time;
        private Text _text;

        private void Start()
        {
            _text = GetComponent<Text>();
            _text.text = Scores.ToString();
            _time = 0;
        }

        private void Update()
        {
            var k = 1 - Mathf.Clamp01(_time / LifeTimeSec);
            _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, k);

            transform.position += new Vector3(0, 10f * Time.deltaTime, 0);

            _time += Time.deltaTime;
            if (_time > LifeTimeSec)
                DestroyImmediate(gameObject);
        }
    }
}