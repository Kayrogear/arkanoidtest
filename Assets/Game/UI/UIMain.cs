﻿using Game.Core;
using Game.Messages;
using UnityEngine;

namespace Game.UI
{
    public class UIMain : MonoSingleton<UIMain>
    {
        private UIGame _uiGame;
        private UIMenu _uiMenu;

        protected override void OnInit()
        {
            DontDestroyOnLoad(gameObject);
            _uiGame = GetComponentInChildren<UIGame>(true);
            _uiMenu = GetComponentInChildren<UIMenu>(true);

            MessagesSystem.AddListener<SceneLoadedMsg>(OnSceneLoaded);
        }

        private void Start()
        {
            SwitchUI(GameApp.Instance.Levels.InMenuScene);
        }

        private void OnDestroy()
        {
            MessagesSystem.RemoveListener<SceneLoadedMsg>(OnSceneLoaded);
        }

        private void OnSceneLoaded(object sender, SceneLoadedMsg m)
        {
            SwitchUI(m.MainMenu);
        }

        private void SwitchUI(bool inMenu)
        {
            _uiMenu.gameObject.SetActive(false);
            _uiGame.gameObject.SetActive(false);

            if (inMenu)
                _uiMenu.gameObject.SetActive(true);
            else
                _uiGame.gameObject.SetActive(true);
        }
    }
}
