﻿using UnityEngine;

namespace Game
{
    public class AnimRandomizer : MonoBehaviour
    {
        public string[] AnimNames;

        void Start()
        {
            var animation = GetComponent<Animation>();
            foreach (var animName in AnimNames)
                animation[animName].time = Random.Range(0, animation[animName].length);
        }
    }
}
