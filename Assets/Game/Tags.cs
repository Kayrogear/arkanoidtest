﻿namespace Game
{
    public static class Tags
    {
        public const string Platform = "Platform";
        public const string Brick = "Brick";
        public const string Ball = "Ball";
        public const string Bonus = "Bonus";
    }
}