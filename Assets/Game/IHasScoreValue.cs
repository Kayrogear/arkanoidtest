﻿namespace Game
{
    public interface IHasScoreValue
    {
        int Scores { get; }
    }
}
