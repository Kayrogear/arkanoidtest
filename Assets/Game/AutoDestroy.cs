﻿using System.Collections;
using UnityEngine;

namespace Game
{
    public class AutoDestroy : MonoBehaviour
    {
        public float LifeTimeSec;

        private void Start()
        {
            StartCoroutine(WaitDestroy());
        }

        private IEnumerator WaitDestroy()
        {
            yield return new WaitForSeconds(LifeTimeSec);
            Destroy(gameObject);
        }
    }
}


