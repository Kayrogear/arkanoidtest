﻿using System;
using Game.Attributes;
using Game.Core;
using Game.Messages;
using UnityEngine;

namespace Game
{
    [ExecuteInEditMode]
    public class WorldSize : MonoSingleton<WorldSize>
    {
        public Camera WorldCamera { get { return Camera.main; } }

        public Vector2 WorldFieldUnits
        {
            get
            {
                var height = WorldCamera.orthographicSize * 2;
                var width = WorldCamera.orthographicSize * 2 * DesignAspect;
                return new Vector2(width, height);
            }
        }

        public Bounds PlayFieldUnits
        {
            get
            {
                var playFeild = new Bounds(Vector3.zero, WorldFieldUnits);
                playFeild.min += new Vector3(DesignLeftPlayOffset, DesignBottomPlayOffset) / _designPixelsPerUnit;
                playFeild.max += new Vector3(-DesignRightPlayOffset, -DesignTopPlayOffset) / _designPixelsPerUnit;
                return playFeild;
            }
        }

        public Bounds PlayFieldRealUnits
        {
            get
            {
                var playFielsUnits = PlayFieldUnits;
                playFielsUnits.min *= WorldScale;
                playFielsUnits.max *= WorldScale;
                return playFielsUnits;
            }
        }

        public Vector2 WorldFieldRealUnits { get { return WorldFieldUnits * WorldScale; } }
        public float DesignAspect { get { return (float)_designWidth / _designHeight; } }
        public float WorldScale { get; private set; }
        public float LostLevelY { get { return -WorldFieldUnits.y / 2 + _lostLevelOffsetY; } }

        [SerializeField]
        private int _designWidth;

        [SerializeField]
        private int _designHeight;

        [SerializeField]
        private float _lostLevelOffsetY;

        public int DesignRightPlayOffset;
        public int DesignLeftPlayOffset;
        public int DesignTopPlayOffset;
        public int DesignBottomPlayOffset;

        [ReadOnly]
        [SerializeField]
        private string _designAspect;

        [ReadOnly]
        [SerializeField]
        private float _designPixelsPerUnit;
    
        private int _prevDesignWidth;
        private int _prevDesignHeight;
        private int _camWidth;
        private int _camHeight;
        private float _camOrthoSize;
    
        private void Update()
        {
            if (_camWidth == WorldCamera.pixelWidth &&
                _camHeight == WorldCamera.pixelHeight &&
                _prevDesignWidth == _designWidth &&
                _prevDesignHeight == _designHeight &&
                Math.Abs(_camOrthoSize - WorldCamera.orthographicSize) < 0.0001f)
                return;

            _camWidth = WorldCamera.pixelWidth;
            _camHeight = WorldCamera.pixelHeight;
            _camOrthoSize = WorldCamera.orthographicSize;
            _prevDesignWidth = _designWidth;
            _prevDesignHeight = _designHeight;


            var camAspect = (float)WorldCamera.pixelWidth / WorldCamera.pixelHeight;
        
            WorldScale = camAspect < DesignAspect
                ? camAspect / DesignAspect
                : 1.0f;

            // debug info
            var aspect = CalcAspectRatio(DesignAspect);
            _designPixelsPerUnit = new Vector2(_designWidth, _designHeight).magnitude / WorldFieldUnits.magnitude;
            _designAspect = string.Format("{0}:{1} ({2})", aspect.x, aspect.y, DesignAspect);

            transform.localScale = Vector3.one * WorldScale;

            MessagesSystem.Send(this, new WorldSizeChangedMsg(this));

            Debug.LogFormat("World Size Changed: {0}x{1} aspect:{2} ({4}:{5}), need rescale: {3}",
                _camWidth, _camHeight, camAspect, camAspect < DesignAspect,
                aspect.x, aspect.y);
        }

        private Vector2 CalcAspectRatio(float aspect)
        {
            for (int n = 1; n < 20; ++n)
            {
                int m = (int)(aspect * n + 0.5); // rounding
                if (Math.Abs(aspect - (double)m / n) < 0.01) 
                    return new Vector2(m,n);
            }
            return Vector2.zero;
        }

        private void OnDrawGizmos()
        {
            var worldFieldReal = WorldFieldRealUnits;
            var playFieldReal = PlayFieldRealUnits;
            
            DrawBoundsGizmo(new Bounds(Vector3.zero, worldFieldReal), Color.blue);
            DrawBoundsGizmo(playFieldReal, Color.green);
            
            var realLostLevelY = LostLevelY * WorldScale;
            var pLost1 = new Vector3(-worldFieldReal.x / 2, realLostLevelY, 0);
            var pLost2 = new Vector3(worldFieldReal.x / 2, realLostLevelY, 0);
            Debug.DrawLine(pLost1, pLost2, Color.red);
        }

        private void DrawBoundsGizmo(Bounds bounds, Color color)
        {
            var p1 = new Vector3(bounds.min.x, bounds.min.y, 0);
            var p2 = new Vector3(bounds.max.x, bounds.min.y, 0);
            var p3 = new Vector3(bounds.max.x, bounds.max.y, 0);
            var p4 = new Vector3(bounds.min.x, bounds.max.y, 0);
            
            Debug.DrawLine(p1, p2, color);
            Debug.DrawLine(p2, p3, color);
            Debug.DrawLine(p3, p4, color);
            Debug.DrawLine(p4, p1, color);
        }
    }
}
