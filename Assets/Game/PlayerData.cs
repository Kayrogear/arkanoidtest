﻿namespace Game
{
    public class PlayerData
    {
        public int Scores { get; set; }
        public int Lives { get; set; }
    }
}
