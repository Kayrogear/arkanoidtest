﻿using Game.Core;
using Game.Messages;
using UnityEngine;

namespace Game
{
    public class GameApp : MonoSingleton<GameApp>
    {
        public LevelLoader Levels { get; private set; }
        public PlayerData Player { get; private set; }

        protected override void OnInit()
        {
            DontDestroyOnLoad(gameObject);
            Levels = GetComponent<LevelLoader>();
            Player = new PlayerData();

            MessagesSystem.AddListener<GameWorldPlayEndMsg>(OnGameWorldPlayEnd);
        }

        private void OnDestroy()
        {
            MessagesSystem.RemoveListener<GameWorldPlayEndMsg>(OnGameWorldPlayEnd);
        }

        private void OnGameWorldPlayEnd(object sender, GameWorldPlayEndMsg m)
        {
        }
    }
}
