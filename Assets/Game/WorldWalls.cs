﻿using Game.Core;
using Game.Messages;
using UnityEngine;

namespace Game
{
    [ExecuteInEditMode]
    public class WorldWalls : MonoBehaviour
    {
        public GameObject WallLeft;
        public GameObject WallRight;
        public GameObject WallTop;
        public GameObject RedZone;
        public float RedZoneOffset;

        private void Start()
        {
	        MessagesSystem.AddListener<WorldSizeChangedMsg>(OnWorldSizeChanged);
            UpdateWalls(WorldSize.Instance);
        }

        private void OnDestroy()
        {
            MessagesSystem.RemoveListener<WorldSizeChangedMsg>(OnWorldSizeChanged);
        }

        private void UpdateWalls(WorldSize worldSize)
        {
            var playFieldUnits = worldSize.PlayFieldUnits;
            WallLeft.transform.localPosition = new Vector3(playFieldUnits.min.x, 0, 0);
            WallRight.transform.localPosition = new Vector3(playFieldUnits.max.x, 0, 0);
            WallTop.transform.localPosition = new Vector3(0, playFieldUnits.max.y, 0);
            RedZone.transform.localPosition = new Vector3(0, playFieldUnits.min.y + RedZoneOffset, 0);
        }

        private void OnWorldSizeChanged(object sender, WorldSizeChangedMsg m)
        {
            UpdateWalls(m.WorldSize);
        }

    }
}
