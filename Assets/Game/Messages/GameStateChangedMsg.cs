﻿using Game.Core;

namespace Game.Messages
{
    public class GameStateChangedMsg : MessageBase
    {
        public GameWorldState PrevState { get; private set; }
        public GameWorldState NewState { get; private set; }

        public GameStateChangedMsg(GameWorldState prevState, GameWorldState newState)
        {
            PrevState = prevState;
            NewState = newState;
        }
    }
}
