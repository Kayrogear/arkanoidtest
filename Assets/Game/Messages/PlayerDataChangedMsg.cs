﻿using Game.Core;

namespace Game.Messages
{
    public class PlayerDataChangedMsg : MessageBase
    {
        public PlayerData Player { get; private set; }
        
        public PlayerDataChangedMsg(PlayerData player)
        {
            Player = player;
        }
    }
}
