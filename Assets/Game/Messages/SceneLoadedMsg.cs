﻿using Game.Core;

namespace Game.Messages
{
    public class SceneLoadedMsg : MessageBase
    {
        public bool MainMenu { get; private set; }
        public int LevelIndex { get; private set; }

        public SceneLoadedMsg(bool mainMenu, int levelIndex)
        {
            MainMenu = mainMenu;
            LevelIndex = levelIndex;
        }
    }
}
