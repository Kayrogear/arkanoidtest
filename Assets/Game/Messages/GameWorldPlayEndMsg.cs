﻿using Game.Core;

namespace Game.Messages
{
    public class GameWorldPlayEndMsg : MessageBase
    {
        public bool Complete { get; set; }
        
        public GameWorldPlayEndMsg(bool complete)
        {
            Complete = complete;
        }
    }
}
