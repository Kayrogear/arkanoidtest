﻿using Game.Core;
using UnityEngine;

namespace Game.Messages
{
    public class WorldSizeChangedMsg : MessageBase
    {
        public WorldSize WorldSize { get; set; }
        
        public WorldSizeChangedMsg(WorldSize worldSize)
        {
            WorldSize = worldSize;
        }
    }
}
