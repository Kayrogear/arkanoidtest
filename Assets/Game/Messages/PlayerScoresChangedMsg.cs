﻿using Game.Actors;
using Game.Core;

namespace Game.Messages
{
    public class PlayerScoresChangedMsg : MessageBase
    {
        public PlayerData Player { get; private set; }
        public int ScoresValue { get; private set; }
        public GameActor ByActor { get; set; }

        public PlayerScoresChangedMsg(PlayerData player, int scoresValue, GameActor byActor)
        {
            Player = player;
            ScoresValue = scoresValue;
            ByActor = byActor;
        }
    }
}
