﻿using Game.Core;

namespace Game.Messages
{
    public class GameWorldPausedMsg : MessageBase
    {
        public bool Paused { get; private set; }

        public GameWorldPausedMsg(bool paused)
        {
            Paused = paused;
        }
    }
}
